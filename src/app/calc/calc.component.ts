import { Component } from '@angular/core';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrl: './calc.component.css'
})

export class CalcComponent {
  title = 'Aplicación de Calculadora';
  operandoA: number = 3;
  operandoB: number = 2;
  res: number = 0;

  sumar(): void {
    if (this.operandoA == 0 && this.operandoB == 0) {
      alert("No se puede sumar 0 + 0");
    }
    else {
    this.res = this.operandoA + this.operandoB;
    }
}
  
}